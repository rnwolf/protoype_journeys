from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time
import proxy
import header_modifier
from pathlib import Path
from ruamel.yaml import YAML
from datetime import datetime
from skimage.metrics import structural_similarity
import cv2
import numpy as np
from string import Template
from PIL import Image


# Use basic in-built templating to create a HTML report.
template_report = Template(open('Template_report.html').read())
template_journey = Template(open('Template_journey.html').read())
template_row = Template(open('Template_row.html').read())
template_column = Template(open('Template_column.html').read())
OUTPUT_DIR = "reports"

yaml = YAML(typ='safe')
config_data = yaml.load(Path("config.yaml"))

selenium_proxy = webdriver.Proxy()


def get_screenshot(url: str=None, screenshot_filename: str=None):
    """
    Taking appropriately sized screenshots requires a double pass approach.
    The first time we open the browser and findout what the page height is.
    Then open the page up in a browser that has been sized to the given height, and then take a screenshot.

    Assumes that we are using Mozilla Firefox as the browser.

    :param url: Url of page to screenshot
    :param screenshot_filename:  File name.
    :return: True if screenshot taken and saved.
    """
    if not ((url is None) or (screenshot_filename is None)):

        from selenium.webdriver import DesiredCapabilities

        capabilities = DesiredCapabilities.FIREFOX
        selenium_proxy.add_to_capabilities(capabilities)
        driver = webdriver.Firefox(capabilities=capabilities)
        driver.get(url)
        time.sleep(3)
        # get scroll Height
        height = driver.execute_script(
            "return Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight )")
        # print(height)
        driver.close() # Close browser

        # Open another headless browser with height extracted above
        firefox_options = Options()
        firefox_options.add_argument("--headless")
        firefox_options.add_argument(f"--width=1920")
        firefox_options.add_argument(f"--height={height}")
        firefox_options.add_argument("--hide-scrollbars")
        driver = webdriver.Firefox(options=firefox_options)
        driver.get(url)

        # pause 3 second to let page load
        time.sleep(3)

        # save screenshot
        screenshotfile = driver.save_screenshot(screenshot_filename)

        driver.quit()
        return screenshotfile
    else:
        return False

def diff_images(before_filename,after_filename):
    """
    Make it easy to visually see the differences between two similar images.
     
    Compare two equally sized images and for each create and image circling the differences.
     
    :param before_filename: 
    :param after_filename: 
    :return: 
    """
    before = cv2.imread(before_filename)
    after = cv2.imread(after_filename)

    # Convert images to grayscale
    before_gray = cv2.cvtColor(before, cv2.COLOR_BGR2GRAY)
    after_gray = cv2.cvtColor(after, cv2.COLOR_BGR2GRAY)

    # Compute SSIM between two images
    # (score, diff) = compare_ssim(before_gray, after_gray, full=True)
    (score, diff) = structural_similarity(before_gray, after_gray, full=True)

    #print("Image similarity", score)

    # The diff image contains the actual image differences between the two images
    # and is represented as a floating point data type in the range [0,1]
    # so we must convert the array to 8-bit unsigned integers in the range
    # [0,255] before we can use it with OpenCV
    diff = (diff * 255).astype("uint8")

    # Threshold the difference image, followed by finding contours to
    # obtain the regions of the two input images that differ
    thresh = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]

    mask = np.zeros(before.shape, dtype='uint8')
    filled_after = after.copy()

    for c in contours:
        area = cv2.contourArea(c)
        if area > 40:
            x, y, w, h = cv2.boundingRect(c)
            cv2.rectangle(before, (x, y), (x + w, y + h), (36, 255, 12), 2)
            cv2.rectangle(after, (x, y), (x + w, y + h), (36, 255, 12), 2)
            cv2.drawContours(mask, [c], 0, (0, 255, 0), -1)
            cv2.drawContours(filled_after, [c], 0, (0, 255, 0), -1)

    cv2.imwrite(before_filename, before)
    cv2.imwrite(after_filename, after)
    return


if __name__ == '__main__':
    from proxy.common import utils

    proxy_port = utils.get_available_port()
    with proxy.start(
            ['--host', '127.0.0.1',
             '--port', str(proxy_port),
             '--ca-cert-file', 'wec-ca.pem',
             '--ca-key-file', 'wec-ca.key',
             '--ca-signing-key-file', 'wec-signing.key'],
            plugins=
            [b'header_modifier.BasicAuthorizationPlugin',
             header_modifier.BasicAuthorizationPlugin]):
        from selenium.webdriver.common.proxy import ProxyType

        selenium_proxy.proxyType = ProxyType.MANUAL
        selenium_proxy.httpProxy = '127.0.0.1:' + str(proxy_port)
        selenium_proxy.sslProxy = '127.0.0.1:' + str(proxy_port)
        print('Proxy address: ' + selenium_proxy.httpProxy)

        report_journeys = ""
        for journey in config_data['Journeys']:
            #print(journey)
            report_rows = ""
            for page_name in config_data['Journeys'][journey]:
                #print(page_name)
                latest_page_version = None
                previous_page_version = None
                for page_version in config_data['Journeys'][journey][page_name]:
                    if latest_page_version is None:
                        latest_page_version = page_version
                    elif previous_page_version is None:
                        previous_page_version = page_version
                    else:  # Break out of the loop. We should have at most two versions that we are comparing.
                        break

                    page_route = config_data['Journeys'][journey][page_name][page_version]['route']
                    url = f"{config_data['Credentials']['base_url']}" + page_route
                    screenshot_filename_base = Path.cwd() / OUTPUT_DIR / f"{datetime.today().strftime('%Y-%m-%d')}_{journey}_{page_name}"
                    screenshot_filename = f"{screenshot_filename_base}_{page_version}.png"
                    get_screenshot(url, screenshot_filename)
                # If there are two versions and make sure they are the same size so we can compare them better
                #print(latest_page_version)
                #print(previous_page_version)
                if all([latest_page_version,previous_page_version]): # Skip if no versions
                    #We have two versions
                    latest_images_filename =f"{screenshot_filename_base}_{latest_page_version}.png"
                    previous_image_filename=f"{screenshot_filename_base}_{previous_page_version}.png"
                    latest_image = Image.open(latest_images_filename)
                    previous_image = Image.open(previous_image_filename)
                    latest_height = latest_image.height
                    previous_height = previous_image.height
                    if latest_height != previous_height:
                        new_image = Image.new(latest_image.mode, (latest_image.width, max([latest_height,previous_height])))
                        if latest_height == max([latest_height,previous_height]):
                            # Then it must be the previous version that is smaller.
                            # Copy the previous version screen shot onto the bigger canvas.
                            new_image.paste(previous_image,(0,0))
                            new_image.save(previous_image_filename)
                        else:
                            # latest version is not the larger image, copy the later screen shot onto the bigger canvas
                            new_image.paste(latest_image,(0,0))
                            new_image.save(latest_images_filename)
                    diff_images(previous_image_filename,latest_images_filename)

                    latest_report_column = template_column.safe_substitute(cell_text=page_name, image_file_name=latest_images_filename)
                    previous_report_column = template_column.safe_substitute(cell_text=page_name, image_file_name=previous_image_filename)
                report_row = template_row.safe_substitute(latest_column=latest_report_column,previous_column=previous_report_column)
                report_rows += report_row
            report_journey = template_journey.safe_substitute(journey=journey, table_rows=report_rows)
            report_journeys += report_journey
        # After all the journeys write out a report file
        report = template_report.safe_substitute(date=f"{datetime.today().strftime('%Y-%m-%d')}",journey_tables=report_journeys)
        # Write the report out to a file.
        report_file_name = Path.cwd() / OUTPUT_DIR / f"{datetime.today().strftime('%Y-%m-%d')}-Prototype_journeys_report.html"
        f = open(report_file_name, "w")
        f.write(report)
        f.close()
