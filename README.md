# README

## What?

Automate the creation of screenshots joined up into a versioned journey.

Differences between pages are made easy to see by outlining areas of change with bright green lines.

Use case: Versioned HTML prototype site behind basic auth. Take configurable sequence screenshots in a user journey. Quickly see which pages have changed between versions.

## Quick start

1. Download the repo.
2. Create a Python 3.8 virtualenv and activate it.

3. Install dependencies `pip install -r requirements.txt`

4. Copy the example config file to `config.yaml` and update it.

5. Install Mozilla FireFox

6. Install the GeckoDriver for your version of FireFox https://github.com/mozilla/geckodriver as we are using selenium to operate FireFox from our main Python script.

7. Install openssl as we will be adding HTTPs headers will require openssl tool

- Generate CA private key file:

`openssl genrsa -out wec-ca.key 2048`

- Generate CA root certificate:

`openssl req -x509 -new -nodes -key wec-ca.key -sha256 -days 1825 -out wec-ca.pem`

- Generate private key file for server certificate generation:

`openssl genrsa -out wec-signing.key 2048`

Now you should have three files.
We use a proxy plugin passes all the traffic through itself hence the headers are added to all the requests which go from your browser.

8. Run `python main.py`

- You should see FireFox open up every now and then.

- Your screenshots and report should be in the reports subdirectory.

# TODO

1. Refactor to allow for the case were pages are in the latest prototype version and not the previous one. And the inverse situation.
