from proxy.http.proxy import HttpProxyBasePlugin
from proxy.http.parser import HttpParser
from typing import Optional
import base64
from pathlib import Path
from ruamel.yaml import YAML

yaml = YAML(typ='safe')
config_data = yaml.load(Path("config.yaml"))

class BasicAuthorizationPlugin(HttpProxyBasePlugin):
    """Modifies request headers."""

    def before_upstream_connection(
            self, request: HttpParser) -> Optional[HttpParser]:
        return request

    def handle_client_request(
            self, request: HttpParser) -> Optional[HttpParser]:
        #basic_auth_header = 'Basic ' + base64.b64encode('webelement:click'.encode('utf-8')).decode('utf-8')
        basic_auth_header = 'Basic ' + base64.b64encode(f"{config_data['Credentials']['username']}:{config_data['Credentials']['password']}".encode('utf-8')).decode('utf-8')
        request.add_header('Authorization'.encode('utf-8'), basic_auth_header.encode('utf-8'))
        return request

    def on_upstream_connection_close(self) -> None:
        pass

    def handle_upstream_chunk(self, chunk: memoryview) -> memoryview:
        return chunk